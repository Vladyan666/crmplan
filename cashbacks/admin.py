# -*- coding: utf-8 -*-
from django.contrib import admin

from cashbacks.models import *

# Register your models here.
page_fields = [
    (u"Настройки страницы" , {'fields':['menutitle','alias','menushow','sitemap']}),
    (u"SEO информация" , {'fields':['seo_h1','seo_title','seo_description','seo_keywords','content',]})
]
page_list = ('menushow','sitemap')

#Страницы
class TextPageAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Основные',  {'fields':['name', 'menuposition',]}),] + page_fields
    
admin.site.register(TextPage, TextPageAdmin)

class ServiceAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Основные',  {'fields':['name', 'menuposition',]}),] + page_fields

admin.site.register(Service, ServiceAdmin)

class AccountAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['name', 'service', 'login', 'password', 'mail', 'mailpassword', 'balance', 'currency', 'phone', 'status', ]}),]
    list_filter = ['service']

admin.site.register(Account, AccountAdmin)
    
