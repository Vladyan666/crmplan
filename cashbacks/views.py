import django
import json
import re
import time
import requests
import hashlib
from collections import OrderedDict
import itertools
from operator import itemgetter
from django.db.models import Q
from datetime import datetime, timedelta
from django.http import HttpResponse
from django.template.context_processors import csrf
from django.http.response import Http404
from django.forms.models import model_to_dict
from django.shortcuts import render, render_to_response, get_object_or_404, redirect
from django.template.loader import render_to_string
from django.template import RequestContext, loader
from django.conf import settings as django_settings
from django.core import serializers
from django.contrib.sitemaps import Sitemap
from django.contrib.sitemaps.views import x_robots_tag
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

from .models import *

def default_context(request):
    
    c = {}
    c.update(csrf(request))
    
    context_object = {
        'c': c,
    }
    return context_object

def index(request):
    
    context = default_context(request)
    
    template = loader.get_template('cash/index.html')
    
    statuses = []
    
    services = Service.objects.all()

    for s in services:
        for i in ACCOUNT_STATUS:
            statuses.append([i[0], Account.objects.filter(status=i[0], service__name=s.name).count(), s.name, s.pk])
    
    
    context.update({
        'services' : services,
        'statuses' : statuses
    })
    
    return HttpResponse(template.render(context))
