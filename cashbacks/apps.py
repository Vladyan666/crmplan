from django.apps import AppConfig


class CashbacksConfig(AppConfig):
    name = 'cashbacks'
