from __future__ import unicode_literals

import datetime
import json
from django.db import models
from django.utils import timezone
from django import forms
from django.contrib.postgres.fields import JSONField
from django.contrib.auth.models import User, Group


# Create your models here.
class Page(models.Model):
    class Meta:
        abstract = True
        
    seo_h1 = models.CharField( max_length=200 , verbose_name="H1" , null=True , blank=True )
    seo_title = models.CharField( max_length=200 , verbose_name="Title" , null=True , blank=True )
    seo_description = models.CharField( max_length=500 , verbose_name="Description" , null=True , blank=True )
    seo_keywords = models.CharField( max_length=200 , verbose_name="Title" , null=True , blank=True )
    alias = models.SlugField( max_length=200 , verbose_name=u"Псевдоним", null=True , blank=True)
    menutitle = models.CharField( max_length=200 , verbose_name=u"Название в меню" , null=True , blank=True )
    menushow = models.BooleanField( default=True, verbose_name=u"Показывать в меню" )
    menuposition = models.IntegerField( verbose_name=u"Позиция в меню", null=True , blank=True )
    sitemap = models.BooleanField( default=True , verbose_name=u"Показывать в карте сайта" )
    content = models.TextField( verbose_name=u"Статья" , null=True , blank=True )

            
class TextPage(Page):
    class Meta:
        verbose_name = u"Текстовая страница"
        verbose_name_plural = u"Страницы"
        
    name = models.CharField( max_length=200 , verbose_name=u"Название" )
    
    def __str__(self):
        return self.name
        
class Service(Page):
    class Meta:
        verbose_name = u"Сервис"
        verbose_name_plural = u"Сервисы"
        
    name = models.CharField( max_length=200 , verbose_name=u"Название" )
    
    def __str__(self):
        return self.name
        
#accstatus
ACCOUNT_STATUS = (
    ('forcheck', 'forcheck'),
    ('inprogress', 'inprogress'),
    ('zero', 'zero'),
    ('softerror', 'softerror'),
    ('mailnotfound', 'mailnotfound'),
    ('mailnotvalid', 'mailnotvalid'),
    ('waiting', 'waiting'),
    ('goodphone', 'goodphone'),
    ('good', 'good'),
    ('unending reset', 'unending reset'),
    ('error', 'error'),
    ('server not connected', 'server not connected'),
    ('done', 'done'),
)
        
class Account(models.Model):
    class Meta:
        verbose_name = u"Аккаунт"
        verbose_name_plural = u"Аккаунты"
        
    name = models.CharField( max_length=200 , verbose_name=u"Название")
    checkproxy = models.CharField( max_length=200 , verbose_name=u"Прокси проверки" , null=True , blank=True )
    withdrawaproxy = models.CharField( max_length=200 , verbose_name=u"Прокси вывода" , null=True , blank=True )
    service = models.ForeignKey(Service, verbose_name='Сервис')
    login = models.CharField(max_length=200, verbose_name='Логин', null=True , blank=True)
    password = models.CharField(max_length=200, verbose_name='Пароль', null=True , blank=True)
    mail = models.CharField(max_length=200, verbose_name='Почта')
    mailpassword = models.CharField(max_length=200, verbose_name='Пароль от почты')
    mailfilter = models.CharField(max_length=200, verbose_name='Фильтр', null=True , blank=True)
    balance = models.FloatField(default=0, verbose_name='Баланс', null=True , blank=True )
    pending = models.FloatField(default=0, verbose_name='В обработке', null=True , blank=True )
    currency = models.CharField(max_length=200, verbose_name='Валюта', null=True , blank=True )
    phone = models.CharField(max_length=200, verbose_name='Телефон', null=True , blank=True )
    createdate = models.DateTimeField(auto_now_add=True, verbose_name = 'Время создания проверки', null=True , blank=True)
    checkdate = models.DateTimeField(auto_now_add=True, verbose_name = 'Время первой проверки', null=True , blank=True)
    moneydate = models.DateTimeField(verbose_name = 'Время первой проверки', null=True , blank=True)
    status = models.CharField(max_length=200, verbose_name='Статус', null=True , blank=True)
    
    def __str__(self):
        return self.mail