from django.contrib import admin
from django.conf.urls import url, include
from django.contrib.auth.models import User
from . import views
app_name = 'monitoring'


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^$', views.mainpage, name='mainpage'),
]