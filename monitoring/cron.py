print('starting')
import os
import sys
import re, requests, json, time
import whois
import random
import OpenSSL
import ssl, socket

import django
from django.apps import apps

# reload(sys)  
# sys.setdefaultencoding('utf8')

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "crmplan.settings")
django.setup()

from monitoring.views import get_domains_info
from monitoring.models import *

print('take doorway domains')
doorways = eval(requests.get('https://crocus-city-hall.ru/api-2c74fd/sites/').content)

print('create or update doorway domains')
for i in doorways:
    obj, created = Domain.objects.get_or_create(name=i, domain=i)

print('going to the check')
get_domains_info(1)
print('end')