from __future__ import unicode_literals

import datetime
import json
from django.db import models
from django.utils import timezone
from django import forms
from django.contrib.postgres.fields import JSONField
from django.contrib.auth.models import User, Group


# Create your models here.

SITE_CATEGORY = (
    (1, 'Doorway'),
    (2, 'Sport'),
    (3, 'Other')
)

def get_days_names(num):
    if num == 0:
        return 'Меньше дня'
    elif num%10 == 1:
        return '%s день' % num
    elif num%10 in [2,3,4]:
        return '%s дня' % num
    else:
        return '%s дней' % num

        
class Domain(models.Model):
    class Meta:
        verbose_name = u"Домен"
        verbose_name_plural = u"Домены"
        
    name = models.CharField( max_length=40, verbose_name="Домен в произвольном виде или название проекта" )
    domain = models.CharField( max_length=40, verbose_name="Домен вида site.com" )
    access = models.TextField( max_length=500, verbose_name="Доступы", null=True, blank=True )
    statushttps = models.CharField( max_length=40, verbose_name="Статус https", null=True, blank=True )
    statushttp = models.CharField( max_length=40, verbose_name="Статус http", null=True, blank=True )
    statuswww = models.CharField( max_length=40, verbose_name="Статус www", null=True, blank=True )
    statusnotwww = models.CharField( max_length=40, verbose_name="Статус notwww", null=True, blank=True )
    certify = models.BooleanField(default=False, verbose_name="Сертификат")
    certify_end = models.DateTimeField(verbose_name=u'Дата истечения сертификаты', null=True, blank=True)
    whois = JSONField( default=list, verbose_name="Whois информация", null=True, blank=True )
    ip = models.CharField( max_length=40, verbose_name="IP", null=True, blank=True )
    domain_category = models.PositiveSmallIntegerField(verbose_name='Категория', help_text='Примерно хотябы к чему относится', default=3, choices=SITE_CATEGORY)
    
    def status_badge_https(self):
        if self.statushttps:
            if int(self.statushttps) in range(200,299):
                return 'success'
            elif int(self.statushttps) in range(300,399):
                return 'warning'
            elif int(self.statushttps) in range(400,600):
                return 'danger'
            else:
                return 'primary'
        else:
            return 'primary'
            
    def status_badge_http(self):
        if self.statushttp:
            if int(self.statushttp) in range(200,299):
                return 'success'
            elif int(self.statushttp) in range(300,399):
                return 'warning'
            elif int(self.statushttp) in range(400,600):
                return 'danger'
            else:
                return 'primary'
        else:
            return 'primary'
            
    def status_badge_www(self):
        if self.statuswww and self.statusnotwww:
            if int(self.statuswww) in range(200,299) and int(self.statusnotwww) in range(300,399) or int(self.statuswww) in range(300,399) and int(self.statusnotwww) in range(200,299):
                return 'success'
            else:
                return 'warning'
        else:
            return 'warning'
            
    def domain_days_to_go(self):
        if self.whois:
            duration = datetime.datetime.strptime(self.whois[0]['expiration_date'], '%d-%m-%Y %H:%M').replace(tzinfo=None) - datetime.datetime.now().replace(tzinfo=None)
            return [get_days_names(duration.days), duration.days]
            
    def cert_days_to_go(self):
        if self.certify_end:
            duration = self.certify_end.replace(tzinfo=None) - datetime.datetime.now().replace(tzinfo=None)
            return [get_days_names(duration.days), duration.days]
        
    def __str__(self):
        return self.name