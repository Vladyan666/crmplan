import django
import json
import os
import re
import sys
import uuid
import requests
import whois
import random
from urllib.request import Request, urlopen, ssl, socket
from urllib.error import URLError, HTTPError
import ssl, socket
from collections import OrderedDict
import itertools
from interruptingcow import timeout
from operator import itemgetter
from django.db.models import Q
from datetime import datetime
from django.http import HttpResponse
from django.template.context_processors import csrf
from django.http.response import Http404
from django.forms.models import model_to_dict
from django.shortcuts import render, render_to_response, get_object_or_404, redirect
from django.template.loader import render_to_string
from django.template import RequestContext, loader
from django.conf import settings as django_settings
from django.core import serializers
from django.contrib.sitemaps import Sitemap
from django.contrib.sitemaps.views import x_robots_tag
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

from .models import *

def mainpage(request):
    
    template = loader.get_template('monitoring/index.html')
    
    domains = Domain.objects.all()
    
    context = {
        'domains' : domains,
    }
    
    return HttpResponse(template.render(context))
    
def get_domains_info(request):
    items = Domain.objects.all()
    for i in items:
        #whois info
        print('whois ' + i.domain)
        try:
            result = whois.query(i.domain)
            i.whois = [{'expiration_date' : result.expiration_date.strftime('%d-%m-%Y %H:%M'), 'creation_date' : result.creation_date.strftime('%d-%m-%Y %H:%M'), 'ns' : list(result.name_servers), 'registrar' : result.registrar}]
        except:
            i.whois = []
        #проверки редиректов
        print('redirects ' + i.domain)
        try:
            http = requests.get('http://' + i.domain, verify=False)
            #проверка редиректа на https
            
            if 'https' in http.url.split(':')[0] and http.status_code == 200:
                i.statushttp = '301'
            else:
                i.statushttp = str(http.status_code)
            #обработка если есть редирект на http(безумно но все таки)
            if i.statushttp == '301':
                i.statushttps = str(requests.get('https://' + i.domain, verify=False).status_code)
            else:
                try:
                    i.statushttps = str(requests.get('https://' + i.domain, timeout=3, verify=False).status_code)
                except Exception:
                    i.statushttps = '400'
            #проверка WWW
            if '/www.' in http.url:
                i.statuswww = '301'
            else:
                i.statuswww = '200'
                
            #проверка без WWW
            if '/www.' in requests.get('http://www.' + i.domain, verify=False).url:
                i.statusnotwww = '200'
            else:
                i.statusnotwww = '301'
                
        except Exception:
                i.statushttp = '400'
                i.statushttps = '400'
                i.statuswww = '400'
                i.statusnotwww = '400'
        #проверка сертификата
        print('certify ' + i.domain)
        try:
            with timeout(3, exception=RuntimeError):
                print(i.domain)
                base_url = i.domain
                port = '443'
                hostname = base_url
                context = ssl.create_default_context()
                with socket.create_connection((hostname, port)) as sock:
                    with context.wrap_socket(sock, server_hostname=hostname) as ssock:
                        time_paste = ssock.getpeercert()['notAfter']
                sock.close()
                i.certify = True
                i.certify_end = datetime.datetime.strptime(time_paste, '%b %d %H:%M:%S %Y GMT')
        except Exception:
            i.certify = False
            i.certify_end = None
            
        #get_ip
        print('ip ' + i.domain)
        try:
            i.ip = socket.gethostbyname(i.domain)
        except:
            i.ip = None
        i.save()