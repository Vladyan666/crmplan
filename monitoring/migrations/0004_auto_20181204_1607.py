# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2018-12-04 16:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('monitoring', '0003_auto_20181203_1943'),
    ]

    operations = [
        migrations.AddField(
            model_name='domain',
            name='certify',
            field=models.BooleanField(default=False, verbose_name='Сертификат'),
        ),
        migrations.AddField(
            model_name='domain',
            name='certify_end',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Дата истечения сертификаты'),
        ),
    ]
