# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2018-12-03 18:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Domain',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, verbose_name='Домен в произвольном виде или название проекта')),
                ('domain', models.CharField(max_length=200, verbose_name='Домен вида https://site.com')),
                ('access', models.TextField(max_length=500, verbose_name='Доступы')),
                ('status', models.CharField(max_length=200, verbose_name='Статус')),
                ('whois', models.TextField(max_length=200000, verbose_name='Whois информация')),
            ],
            options={
                'verbose_name': 'Домен',
                'verbose_name_plural': 'Домены',
            },
        ),
    ]
