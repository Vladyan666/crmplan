from django.contrib import admin

from monitoring.models import *


#Роли
class DomainAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['name', 'domain', 'access', 'statushttps','statushttp', 'statuswww', 'statusnotwww', 'ip', 'whois', 'certify', 'certify_end']}),] 

admin.site.register(Domain, DomainAdmin)