from django.apps import AppConfig


class MailbaseConfig(AppConfig):
    name = 'mailbase'
