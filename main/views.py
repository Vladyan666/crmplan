import django
import datetime
import json
import os
import re
import sys
import uuid
import requests
import random
from collections import OrderedDict
import itertools
from operator import itemgetter
from django.db.models import Q
from datetime import datetime
from django.http import HttpResponse
from django.template.context_processors import csrf
from django.http.response import Http404
from django.forms.models import model_to_dict
from django.shortcuts import render, render_to_response, get_object_or_404, redirect
from django.template.loader import render_to_string
from django.template import RequestContext, loader
from django.conf import settings as django_settings
from django.core import serializers
from django.contrib.sitemaps import Sitemap
from django.contrib.sitemaps.views import x_robots_tag
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from .models import *

def index(request):
    if request.user.is_authenticated:
        return redirect('/main')

    template = loader.get_template('index.html')
    
    c = {}
    c.update(csrf(request))
    
    context_data = {
        'c': c,
    }
    
    return HttpResponse(template.render(context_data))
    
def main(request):
    if request.user.is_authenticated:
        template = loader.get_template('main.html')
        user_info = get_object_or_404(Manager, user=request.user)
        user_tasks = Task.objects.filter(task_to=user_info)
        projects = Project.objects.all()
        managers = Manager.objects.all()

        return HttpResponse(template.render({ 
            'user_info': user_info,
            'user_tasks': user_tasks,
            'projects': projects,
            'managers': managers,
            }))
    else:
        return redirect('/')

def go_auth(request):
    if request.is_ajax():
        try:
            user = authenticate(username=request.POST['name'], password=request.POST['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    # request.user.is_authenticated = True    
                return redirect('/main')
            else:
                return HttpResponse('not valid user')
        except Exception:
            return HttpResponse('not valid user')
    else:
        return redirect('/')

def go_logout(request):
    logout(request)
    return redirect('/')

def create_project(request):
    if request.is_ajax():
        try:
            project = Project.objects.create(
                name=request.POST.get('project_name'), 
                description=request.POST.get('project_description'),
                creator=get_object_or_404(Manager, user=request.user),
                create_date=datetime.datetime.now())
            return HttpResponse(str(project.pk))
        except Exception:
            return HttpResponse('error')

def create_user(request):
    if request.is_ajax():
        # try:
            user = User.objects.create_user(
                username=request.POST.get('login'), 
                email=request.POST.get('email'), 
                password=request.POST.get('password'))
            manager = Manager.objects.create(
                user=user,
                name=request.POST.get('username'), 
                surname=request.POST.get('surname'), 
                phone=request.POST.get('phone'),
                info=request.POST.get('info'),
                email=request.POST.get('email'))

            return HttpResponse('ok')
        # except Exception:
        #     return HttpResponse('error')

def create_task(request):
    if request.is_ajax():
        task = Task.objects.create(
            project=get_object_or_404(Project, pk=request.POST.get('taskProject')),
            name=request.POST.get('projectName'),
            task_to=get_object_or_404(Manager, pk=request.POST.get('projectContributor')),
            task_from=get_object_or_404(Manager, user=request.user),
            create_date=datetime.datetime.now(),
            finish_date=datetime.datetime.strptime(request.POST.get('projectFinishDate'), '%d.%m.%Y'),
            comment=request.POST.get('taskComment')
        )
        return HttpResponse('ok')

def format_datetime(date):
    return str(date.strftime("%d.%m %H:%M")) if date != None else date

def format_date(date):
    return str(date.strftime("%d.%m.%Y")) if date != None else date

def format_date_deadline(date):
    current_date = datetime.datetime.now()

    if current_date.date() > date.date():
        return "Просрочена"

    return str(date.strftime("%d.%m.%Y")) if date != None else date

def get_project(request):
    if request.is_ajax():
        # try:
            project = get_object_or_404(Project, pk=request.GET.get('selectedOptionKey'))
            project_tasks = Task.objects.filter(project__pk=request.GET.get('selectedOptionKey'))

            lists = list()
            for task in project_tasks:
                data = dict()
                data['pk'] = task.pk
                data['alias'] = task.alias
                data['create_date'] = format_date(task.create_date)
                data['finish_date'] = format_date_deadline(task.finish_date)
                data['hash'] = task.hash
                data['name'] = task.name
                data['task_from'] = task.task_from.name
                data['task_from_pk'] = task.task_from.pk
                data['task_from_surname'] = task.task_from.surname
                data['task_to'] = task.task_to.name
                data['task_to_surname'] = task.task_to.surname
                data['task_to_pk'] = task.task_to.pk
                data['status'] = task.status
                data['project'] = task.project.name
                data['description'] = task.description
                data['comment'] = task.comment
                lists.append(data)


            project_info = dict()
            project_info['name'] = project.name
            project_info['description'] = project.description
            project_info['pk'] = project.pk
            project_info['creator'] = project.creator.name + " " + project.creator.surname
            project_info['create_date'] = format_date(project.create_date)
            project_info['task_count'] = len(lists)
            project_info['tasks'] = lists

            return HttpResponse(json.dumps(project_info), content_type="application/json")
        # except Exception:
        #     return HttpResponse('error')


def zip_profile_data(profile, user):
    profile_info = dict()

    current_user = get_object_or_404(Manager, user=user)

    profile_tasks = Task.objects.filter(task_to=profile).order_by('pk')
    pointed_tasks = Task.objects.filter(task_from=profile).order_by('pk')

    profile_dict = model_to_dict(profile)

    profile_info['user'] = profile_dict

    lists = list()
    if profile_tasks:
        for task in profile_tasks:
            data = dict()
            data['pk'] = task.pk
            data['alias'] = task.alias
            data['create_date'] = format_date(task.create_date)
            data['finish_date'] = format_date_deadline(task.finish_date)
            data['hash'] = task.hash
            data['name'] = task.name
            data['task_from'] = task.task_from.name
            data['task_from_surname'] = task.task_from.surname
            data['task_from_pk'] = task.task_from.pk
            data['task_to'] = task.task_to.name
            data['task_to_pk'] = task.task_to.pk
            data['task_to_surname'] = task.task_to.surname
            data['status'] = task.status
            data['iswatched'] = task.iswatched
            data['project'] = task.project.name
            data['description'] = task.description
            data['comment'] = task.comment
            data['executor'] = 1 if task.task_to == current_user else 0
            lists.append(data)

    profile_info['tasks'] = lists
        
    lists = list()
    if pointed_tasks:
        for task in pointed_tasks:
            data = dict()
            data['pk'] = task.pk
            data['alias'] = task.alias
            data['create_date'] = format_date(task.create_date)
            data['finish_date'] = format_date_deadline(task.finish_date)
            data['hash'] = task.hash
            data['iswatched'] = task.iswatched
            data['name'] = task.name
            data['task_from'] = task.task_from.name
            data['task_from_pk'] = task.task_from.pk
            data['task_from_surname'] = task.task_from.surname
            data['task_to_pk'] = task.task_to.pk
            data['task_to'] = task.task_to.name
            data['task_to_surname'] = task.task_to.surname
            data['status'] = task.status
            data['project'] = task.project.name
            data['comment'] = task.comment
            data['description'] = task.description
            data['executor'] = 1 if task.task_to == current_user else 0
            lists.append(data)

    profile_info['pointed_tasks'] = lists

    return profile_info

def get_profile(request):
    if request.is_ajax():
        profile_identificator = request.GET.get('id')
        profile = get_object_or_404(Manager, id=profile_identificator)

        profile_info = zip_profile_data(profile, request.user)
        return HttpResponse(json.dumps(profile_info), content_type="application/json")

def get_self_profile(request):
    if request.is_ajax():
        # try:
            profile = get_object_or_404(Manager, user=request.user)
            profile_info = zip_profile_data(profile, request.user)            

            return HttpResponse(json.dumps(profile_info), content_type="application/json")
        # except Exception:
        #     return HttpResponse('error')

def get_all_projects(request):
    if request.is_ajax():
        objects = Project.objects.all()
        
        lists = list()
        for object in objects:
            data = dict()
            data['pk'] = object.pk
            data['name'] = object.name
            data['description'] = object.description
            data['create_date'] = format_date(object.create_date)
            lists.append(data)

        return HttpResponse(json.dumps(lists), content_type="application/json")

def get_all_users(request):
    if request.is_ajax():
        objects = Manager.objects.all()

        lists = list()
        for object in objects:
            data = dict()
            data['pk'] = object.pk
            data['name'] = object.name
            data['surname'] = object.surname
            data['email'] = object.email
            data['info'] = object.info
            lists.append(data)

        return HttpResponse(json.dumps(lists), content_type="application/json")

def get_status(request):
    if request.is_ajax():
        request_body = json.loads(request.body)

        lists = list()
        for task_id in request_body['indices']:
            try:
                obj = get_object_or_404(Task, pk=task_id)

                curr = dict()
                curr['pk'] = obj.pk
                curr['name'] = obj.name
                curr['status'] = obj.status
                lists.append(curr)
            except Http404:
                pass

        return HttpResponse(json.dumps(lists),  content_type="application/json")


def remove_projects(request):
    if request.is_ajax():
        projects = json.loads(request.body)

        for pk in projects['indices']:
            Project.objects.filter(pk=pk).delete()

        return HttpResponse(json.dumps('ok'))

def remove_tasks(request):
    if request.is_ajax():
        tasks = json.loads(request.body)

        for pk in tasks['indices']:
            Task.objects.filter(pk=pk).delete()

        return HttpResponse(json.dumps('ok'))

def remove_user(request):
    if request.is_ajax():
        users = json.loads(request.body)

        for pk in users['indices']:
            Manager.objects.filter(pk=pk).delete()
            Task.objects.filter(task_to=pk).delete()

        return HttpResponse(json.dumps('ok'))

def update_status(request):
    if request.is_ajax():
        try:
            current_manager = get_object_or_404(Manager, user=request.user)

            task_id = request.POST.get('taskId')
            task_status = request.POST.get('taskStatus')

            task_obj = get_object_or_404(Task, pk=task_id)

            if task_obj.task_from == current_manager:
                if task_obj.status == "Создана" or task_obj.status == "Тест" or task_obj.status == 'В процессе':
                    task_obj.status = "Закрыта"
                elif task_obj.status == "Закрыта":
                    task_obj.status = "В процессе"
            elif task_obj.task_to == current_manager:
                if task_obj.status == "Создана":
                    task_obj.status = 'В процессе'
                elif task_obj.status == 'В процессе':
                    task_obj.status = "Тест"
                elif task_obj.status == "Тест":
                    task_obj.status = "В процессе"
        
            
            task_obj.save()

            return HttpResponse(task_obj.status.encode('utf-8'))
        except Exception:
            return HttpResponse('error')


def update_comment(request):
    if request.is_ajax():
        task = request.POST.get('task_id')
        comment = request.POST.get('comment')

        task_obj = get_object_or_404(Task, pk=task)
        task_obj.comment = comment
        task_obj.save()
        return HttpResponse('ok')

def update_description_task(request):
    if request.is_ajax():
        task_obj = get_object_or_404(Task, pk=request.POST.get('task_id'))
        task_obj.description = request.POST.get('description')
        task_obj.save()
        return HttpResponse('ok')

def update_description_project(request):
    if request.is_ajax():
        project = get_object_or_404(Project, pk=request.POST.get('id'))
        project.description = request.POST.get('description')
        project.save()
        return HttpResponse('ok')

def update_deadline_task(request):
    if request.is_ajax():
        task = get_object_or_404(Task, pk=request.POST.get('id'))
        task.finish_date = request.POST.get('deadline')
        task.save()
        return HttpResponse('ok')

def update_task_watch(request):
    if request.is_ajax():
        current_manager = get_object_or_404(Manager, user=request.user)
        task = get_object_or_404(Task, pk=request.POST.get('id'))
        
        if task.task_to == current_manager:
            task.status = 'В процессе'
            task.save()
            return HttpResponse('ok')
        return HttpResponse('no')