from __future__ import unicode_literals

import datetime
import json
from django.db import models
from django.utils import timezone
from django import forms
from django.contrib.postgres.fields import JSONField
from django.contrib.auth.models import User, Group


# Create your models here.
class Page(models.Model):
    class Meta:
        abstract = True
        
    seo_h1 = models.CharField( max_length=200 , verbose_name="H1" , null=True , blank=True )
    seo_title = models.CharField( max_length=200 , verbose_name="Title" , null=True , blank=True )
    seo_description = models.CharField( max_length=500 , verbose_name="Description" , null=True , blank=True )
    seo_keywords = models.CharField( max_length=200 , verbose_name="Title" , null=True , blank=True )
    alias = models.SlugField( max_length=200 , verbose_name=u"Псевдоним", null=True , blank=True)
    menutitle = models.CharField( max_length=200 , verbose_name=u"Название в меню" , null=True , blank=True )
    menushow = models.BooleanField( default=True, verbose_name=u"Показывать в меню" )
    menuposition = models.IntegerField( verbose_name=u"Позиция в меню", null=True , blank=True )
    sitemap = models.BooleanField( default=True , verbose_name=u"Показывать в карте сайта" )
    content = models.TextField( verbose_name=u"Статья" , null=True , blank=True )

            
class TextPage(Page):
    class Meta:
        verbose_name = u"Текстовая страница"
        verbose_name_plural = u"Страницы"
        
    name = models.CharField( max_length=200 , verbose_name=u"Название" )
    
    def __str__(self):
        return self.name
        
class Role(Page):
    class Meta:
        verbose_name = u"Роль"
        verbose_name_plural = u"Роли"
        
    name = models.CharField( max_length=200 , verbose_name=u"Название" )
        
    def __str__(self):
        return self.name
        


class Manager(Page):
    class Meta:
        verbose_name = u"Пользователь"
        verbose_name_plural = u"Пользователи"
    
    hash = models.CharField( max_length=200 , verbose_name=u"Хэш Менеджера" )
    name = models.CharField( max_length=200 , verbose_name=u"Имя" )
    surname = models.CharField( max_length=200 , verbose_name=u"Фамилия" )
    user = models.OneToOneField(User,verbose_name=u"Пользователь", null=True , blank=True, on_delete=models.CASCADE)
    phone = models.CharField( max_length=200 , verbose_name=u"Телефон",null=True , blank=True )
    email = models.CharField( max_length=200 , verbose_name=u"Почта",null=True , blank=True )
    info = models.CharField( max_length=200 , verbose_name=u"Информация",null=True , blank=True )
    role = models.ForeignKey(Role, verbose_name=u'Роль', null=True , blank=True, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.name + " " + self.surname

class Project(Page):
    class Meta:
        verbose_name = u"Проект"
        verbose_name_plural = u"Проекты"
        
    name = models.CharField(max_length=200 , verbose_name=u"Название")
    description = models.CharField(max_length=500, verbose_name=u"Описание", null=True, blank=True)
    creator = models.ForeignKey(Manager, verbose_name=u"Создатель", null=True , blank=True, on_delete=models.CASCADE)
    create_date = models.DateTimeField(verbose_name=u'Дата создания', null=True, blank=True)
    
    def __str__(self):
        return self.name
        
class Task(Page):
    class Meta:
        verbose_name = u"Задача"
        verbose_name_plural = u"Задачи"
        
    hash = models.CharField( max_length=200 , verbose_name=u"Хэш Задачи" )
    name = models.CharField( max_length=200 , verbose_name=u"Заголовок" )
    description = models.CharField(max_length=500, verbose_name=u"Описание", null=True, blank=True)
    task_from = models.ForeignKey(Manager,verbose_name=u"Кто поставил задачу", related_name='task_from', null=True , blank=True, on_delete=models.CASCADE)
    task_to = models.ForeignKey(Manager, verbose_name=u'Кому поставили задачу', related_name='task_to',null=True , blank=True, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, verbose_name=u'Проект',null=True , blank=True, on_delete=models.CASCADE)
    create_date = models.DateTimeField(verbose_name=u'Дата создания',null=True , blank=True)
    finish_date = models.DateTimeField(verbose_name=u'Дедлайн',null=True , blank=True)
    checklist = JSONField(verbose_name=u'Чеклист',default=list,null=True , blank=True)
    chat = JSONField(verbose_name=u'Чат',default=list,null=True , blank=True)
    status = models.CharField(max_length=200 , default='Создана',verbose_name=u'Статус')
    priority = models.CharField(max_length=200 , default='Low',verbose_name=u'Приоритет')
    comment = models.CharField(max_length=200 , verbose_name=u'Коммент',null=True , blank=True)
    iswatched = models.BooleanField(default=False , verbose_name=u"Просмотрена", blank=True)
    
    def __str__(self):
        return self.name