from rest_framework.routers import DefaultRouter

from main import api as drf

# Routers provide an easy way of automatically determining the URL conf.
router = DefaultRouter()
router.register(r'managers', drf.ManagerViewSet)
router.register(r'projects', drf.ProjectViewSet)
router.register(r'event', drf.EventViewSet)
router.register(r'cashback', drf.AccountViewSet)