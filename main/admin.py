# -*- coding: utf-8 -*-
from django.contrib import admin

from main.models import *

# Register your models here.
page_fields = [
    (u"Настройки страницы" , {'fields':['menutitle','alias','menushow','sitemap']}),
    (u"SEO информация" , {'fields':['seo_h1','seo_title','seo_description','seo_keywords','content',]})
]
page_list = ('menushow','sitemap')

#Страницы
class TextPageAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Основные',  {'fields':['name', 'menuposition',]}),] + page_fields
    
admin.site.register(TextPage, TextPageAdmin)

#Роли
class RoleAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Основные',  {'fields':['name', 'menuposition',]}),] + page_fields

admin.site.register(Role, RoleAdmin)

#Проект
class ProjectAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Основные',  {'fields':['name', 'description', 'creator', 'create_date', 'menuposition',]}),] + page_fields

admin.site.register(Project, ProjectAdmin)
   
#Менеджеры
class ManagerAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Основные',  {'fields':['hash', 'name', 'surname', 'phone', 'email', 'info', 'user', 'role' ]}),] + page_fields
    
admin.site.register(Manager, ManagerAdmin)

#Задачи
class TaskAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Основные',  {'fields':['hash', 'name', 'description', 'project', 'iswatched', 'create_date','finish_date','checklist','chat', 'status', 'task_from', 'task_to']}),] + page_fields
    
admin.site.register(Task, TaskAdmin)