from rest_framework import viewsets, status as HTTPstatus
from rest_framework import serializers
from main import models as main
from top50 import models as top50
from cashbacks import models as cash


# Serializers define the API representation.
class ManagerSerializer(serializers.ModelSerializer):
    class Meta:
        model = main.Manager
        fields = ('pk', 'hash', 'name', 'surname')


#Кешбеки
class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = cash.Account
        fields = ('pk', 'name', 'service', 'checkproxy', 'withdrawaproxy', 'login', 'password', 'mail', 'mailpassword', 'mailfilter', 'balance' , 'pending', 'currency', 'phone', 'createdate', 'checkdate', 'moneydate', 'status')
        
    def create(self, validated_data):

        obj, created = cash.Account.objects.update_or_create(name=validated_data.pop('name'), defaults=validated_data)
        
        self.is_created = created
        
        instance = obj
            
        return instance

#Проект
class ProjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = top50.Project
        fields = ('pk', 'title', 'favicon', 'metrika_id', 'webmaster_id')
        
    def create(self, validated_data):

        obj, created = top50.Project.objects.get_or_create(**validated_data)
        
        self.is_created = created
        
        instance = obj
            
        return instance


#поисковый запрос
class QuerySerializer(serializers.ModelSerializer):
    class Meta:
        model = top50.Query
        fields = ('pk', 'text', 'wordstat', 'tag')

    def create(self, validated_data):
        
        obj, created = top50.Query.objects.get_or_create(**validated_data)
        
        self.is_created = created
        
        instance = obj
        
        return instance


#Событие
class EventSerializer(serializers.ModelSerializer):

    querys = QuerySerializer(many=True, required=False)
    
    class Meta:
        model = top50.Event
        fields = ('pk', 'name', 'category', 'queries', 'querys', 'startdate', 'finishdate', 'active')
        
    def create(self, validated_data):
        
        qq = validated_data.pop('querys', None)
        
        obj, created = top50.Event.objects.get_or_create(**validated_data)
        
        queries = []
        queries_2 = []
        
        for_check = top50.Query.objects.all().values_list('text', flat=True)
        
        for i in qq:
            if i.get('text') not in for_check:
                queries.append(top50.Query(text=i.get('text'), wordstat=i.get('wordstat')))
            else:
                queries_2.append(top50.Query.objects.get(text=i.get('text')))
        
        created_queries = top50.Query.objects.bulk_create(queries)
        
        to_add = created_queries + queries_2
        
        obj.queries.add(*to_add)
        obj.save()
        
        self.is_created = created
        
        instance = obj
        
        return instance
        