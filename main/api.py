from django.contrib.auth.models import User
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework import serializers, viewsets
from django.contrib.auth import authenticate, login
from rest_framework import viewsets, status as HTTPstatus
from django.core import serializers as obj_serialize
from django.db import transaction
from monitoring.encoding import force_text

from main import models as main
from top50 import models as top50
from top50 import views as views_top50
from cashbacks import models as cash
from main import serializers as drf_serializers
        

class ProjectViewSet(viewsets.ModelViewSet):
    queryset = top50.Project.objects.all()
    
    serializer_class = drf_serializers.ProjectSerializer
    
    def create(self, request, *args, **kwargs):
        
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        
        serializer_data = serializer.data        
        
        serializer_data.update({
            "is_created": serializer.is_created
        })
        
        return Response(serializer_data, status=HTTPstatus.HTTP_201_CREATED, headers=headers)
        
#accounts cashback
class AccountViewSet(viewsets.ModelViewSet):
    queryset = cash.Account.objects.all()
    
    serializer_class = drf_serializers.AccountSerializer
    
    def create(self, request, *args, **kwargs):
        
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        
        serializer_data = serializer.data        
        
        serializer_data.update({
            "is_created": serializer.is_created
        })
        
        return Response(serializer_data, status=HTTPstatus.HTTP_201_CREATED, headers=headers)
        
    @list_route(methods=['post'])
    def select_accs(self, request):
        objs = cash.Account.objects.filter(status=request.data.get('status'), service=cash.Service.objects.get(name=request.data.get('service')))
        response = obj_serialize.serialize('json', objs)
        return Response(force_text(response))
        
    @list_route(methods=['post'])
    def select_account(self, request):
        try:
            with transaction.atomic():
                objs = cash.Account.objects.select_for_update().filter(status=request.data.get('status'))[:1]
                for i in objs:
                    i.status = 'inprogres'
                    i.save()
                    response = obj_serialize.serialize('json', i)
                    return Response(force_text(response))
        except Exception as e:
            return Response(e)
#         except:
#             return Response('net tut nuhuya')

#top 50 events
class EventViewSet(viewsets.ModelViewSet):
    queryset = top50.Event.objects.all()
    
    serializer_class = drf_serializers.EventSerializer
    
    def create(self, request, *args, **kwargs):
        
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        
        serializer_data = serializer.data
        
        serializer_data.update({
            "is_created": serializer.is_created
        })
        
        return Response(serializer_data, status=HTTPstatus.HTTP_201_CREATED, headers=headers)

    @list_route(methods=['post'])
    def add_to_project(self, request):
        project = top50.Project.objects.get(pk=request.data.get('project'))
        events_to_add = top50.Event.objects.filter(pk__in=request.data.get('event'))
        created_querys = []
        response = obj_serialize.serialize('json', events_to_add)
        for event in events_to_add:
            for item in event.queries.all():
                qpage, created = top50.QueryPage.objects.get_or_create(query=item, project=project, defaults={'event': event})
                created_querys.append(qpage) if created else False
#         project.events.add(tuple(events_to_add.values_list('pk', flat=True)))
#         project.save()
        response = obj_serialize.serialize('json', created_querys)
        return Response(response)
        
    @list_route(methods=['post'])
    def run_parse(self, request):
        views_top50.top50parse(1)
        if 1:
            response = {'response' : 'Ok'}
        else:
            response = {'response' : 'Order not found'}
            
        return Response(response)
        
    @list_route(methods=['post'])
    def events_on(self, request):
        response = top50.Event.objects.filter(pk__in=request.data.get('events')).update(active=True)
            
        return Response(response)
        
    @list_route(methods=['post'])
    def events_off(self, request):
        response = top50.Event.objects.filter(pk__in=request.data.get('events')).update(active=False)
            
        return Response(response)

# ViewSets define the view behavior.
class ManagerViewSet(viewsets.ModelViewSet):
    queryset = main.Manager.objects.all()
    serializer_class = drf_serializers.ManagerSerializer
    
    def create(self, request, *args, **kwargs):    
        serializer = self.serializer_class()
        
        return Response(serializer.data)
        
    @list_route(methods=['get'])
    def cheto_tam(self, request):
        if 1:
            response = {'response' : 'Ok'}
        else:
            response = {'response' : 'Order not found'}
        
        return Response(response)