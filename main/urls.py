"""crmplan URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets
from . import routers
from . import views
app_name = 'main'


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^api/', include(routers.router.urls)),
    url(r'^auth/', views.go_auth, name='auth'),
    url(r'^logout/', views.go_logout, name='logout'),
    url(r'^getproject/', views.get_project, name='get_project'),
    url(r'^createproject/', views.create_project, name='create_project'),
    url(r'^createtask/', views.create_task, name='create_task'),
    url(r'^createuser/', views.create_user, name='create_user'),
    url(r'^getselfprofile/', views.get_self_profile, name='get_self_profile'),
    url(r'^getprojects/', views.get_all_projects, name='get_all_projects'),
    url(r'^getusers/', views.get_all_users, name='get_all_users'),
    url(r'^getprofile/', views.get_profile, name='get_profile'),
    url(r'^getstatus/', views.get_status, name='get_status'),
    url(r'^updatecomment/', views.update_comment, name='update_comment'),
    url(r'^updatedescription/', views.update_description_project, name='update_project_description'),
    url(r'^updatestatus/', views.update_status, name='update_status'),
    url(r'^updatetaskdescription/', views.update_description_task, name='update_task_description'),
    url(r'^updatetaskdeadline/', views.update_deadline_task, name='update_task_deadline'),
    url(r'^updatetaskwatch/', views.update_task_watch, name='update_task_watch'),
    url(r'^removeprojects/', views.remove_projects, name='remove_projects'),
    url(r'^removetasks/', views.remove_tasks, name='remove_tasks'),
    url(r'^removeuser/', views.remove_user, name='remove_user'),
    url(r'^$', views.index, name='index'),
    url(r'^main/', views.main, name='main'),
]

