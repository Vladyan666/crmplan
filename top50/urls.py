from django.contrib import admin
from django.conf.urls import url, include
from django.contrib.auth.models import User
from django.conf import settings as django_settings
from django.conf.urls.static import static

from . import views
app_name = 'top50'


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^$', views.mainpage, name='mainpage'),
    url(r'^all-events/', views.allevents, name='allevents'),
    url(r'^project/(?P<alias>[0-9a-zA-Zа-яА-Я\-_\.]+)/$', views.project, name='project'),
    url(r'^event/(?P<alias>[0-9a-zA-Zа-яА-Я\-_\.\—]+)/$', views.single_event, name='single_event'),
    url(r'^qposition/(?P<alias>[0-9a-zA-Zа-яА-Я\-_\.\—]+)/$', views.qpositions, name='qpositions'),
    url(r'^project/(?P<alias>[0-9a-zA-Zа-яА-Я\-_\.\—]+)/queries/$', views.project_queries, name='project_queries'),
]