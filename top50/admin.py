from django.contrib import admin

from top50.models import *

class ProjectAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['title']}),] 
    
admin.site.register(Project, ProjectAdmin)

#События
class EventAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['name','category','queries','startdate','finishdate', 'active',]})]
    list_display = ('name','startdate','finishdate',)   
admin.site.register(Event, EventAdmin)

#Запросы
class QueryAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['text', 'wordstat',]})]
    list_display = ('text', 'wordstat',)   
admin.site.register(Query, QueryAdmin)

class PositionAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['query', 'url', 'pub_date', 'position', 'metrikacount', 'hash', 'additional',]})]
    list_display = ('url', 'position',)   
admin.site.register(Position, PositionAdmin)

class QueryPageAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['query', 'url', 'project', 'event', 'count',]})]
    list_display = ('query', 'project',)   
admin.site.register(QueryPage, QueryPageAdmin)

class QPositionPageAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['query', 'urls_json', 'pub_date','hash']})]
    list_display = ('query', 'pub_date',)   
admin.site.register(QPosition, QPositionPageAdmin)
