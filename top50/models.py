from __future__ import unicode_literals
import json
from django.template import defaultfilters 
from django.contrib.postgres.fields import JSONField
from datetime import datetime, timedelta

from django.db import models

# Create your models here.
class Query(models.Model):
    text = models.CharField( max_length=200 , verbose_name=u"Поисковый запрос" )
    wordstat = models.CharField(max_length=15, verbose_name=u"Частотность", null=True , blank=True)
    tag = models.CharField(max_length=150, verbose_name=u"Группа", null=True , blank=True)
    
    def __str__(self):
	    return self.text

CATEGORY = (
    ('Другое','Другое'),
    ('Спорт','Спорт'),
    ('Театр','Театр'),
    ('Цирк','Цирк')
)

class Event(models.Model):
    class Meta:
        verbose_name = u"Событие(Event)"
        verbose_name_plural = u"События(Events)"
        
    name = models.CharField(max_length = 200, verbose_name = u'Название события' )
    category = models.CharField(max_length = 20, default='Другое', verbose_name=u"Категория", choices=CATEGORY)
    queries = models.ManyToManyField(Query, verbose_name = u'Запросы', blank=True)
    startdate = models.DateTimeField(verbose_name = u'Дата начала продвижения' , null=True , blank=True )
    finishdate = models.DateTimeField(verbose_name = u'Дата окончания продвижения' , null=True , blank=True )
    active = models.BooleanField( default=False , verbose_name=u"Отоброжать на сайте" )

    def __str__(self):
        return self.name
        
class Event_group(models.Model):
    class Meta:
        verbose_name = u"Группа собыитй(Event)"
        verbose_name_plural = u"Группы событий(Events)"

    name = models.CharField(max_length = 200, verbose_name = u'Название группы событий' )
    events = models.ManyToManyField(Event, verbose_name = u'Собыитя', blank=True)
    
    def __str__(self):
        return self.name 
        
class Project(models.Model):
    class Meta:
        verbose_name = u"Проект"
        verbose_name_plural = u"Проекты"
        
    title = models.CharField(max_length=50, verbose_name=u"Название проекта")
    favicon = models.CharField(max_length=500, verbose_name=u"favicon", null=True, blank=True)
    events = models.ManyToManyField(Event, verbose_name = u'События у проекта', null=True, blank=True)
    metrika_id = models.IntegerField(default=0, verbose_name=u'Ya Metrika id')
    webmaster_id = models.CharField(max_length=200, verbose_name=u'Ya Webmaster id', null=True, blank=True)
       
    def __str__(self):
        return self.title
        
class QueryPage(models.Model):
    class Meta:
        verbose_name = u"Поисковый запрос"
        verbose_name_plural = u"Поисковые запросы"

    
    query = models.ForeignKey(Query, verbose_name=u"Запрос")
    url = models.CharField(max_length=200, verbose_name=u"Url")
    project = models.ForeignKey(Project, verbose_name=u"Проект")
    event = models.ForeignKey(Event, verbose_name=u"Событие", null=True, blank=True)
    count = models.IntegerField(default=0, verbose_name=u'Количество переходов')
    positionss = JSONField(verbose_name=u"Позиции", default=list, null=True, blank=True)

    def current_position(self):
        try:
            item = self.positions.all().order_by('-pub_date', '-id').last()
            result = item.position
        except:
            result = 666
        return result
        
    def last_positions(self):
        until = datetime.now() - timedelta(days=2)
        result = self.positions.all().filter(pub_date__gte=until).order_by('-pub_date', '-id')
        return result

    def __str__(self):
        return self.query.text

class Position(models.Model):
    class Meta:
        verbose_name = u"Позиция"
        verbose_name_plural = u"Позиции"

    query = models.ForeignKey(QueryPage, verbose_name=u"Поисковый запрос", related_name="positions")
    additional = JSONField(verbose_name=u"Доп позиции", null=True, blank=True)
    url = models.CharField(max_length=200, verbose_name=u"Url", null=True, blank=True)
    pub_date = models.DateField('Дата снятия')
    position = models.IntegerField(default="666", verbose_name=u"Позиция")
    metrikacount = models.IntegerField(default=0, verbose_name=u'Заходы по метрике')
    hash = models.CharField(max_length=32, verbose_name=u"Hash", unique=True)

    def __str__(self):
        return self.query.query.text 
        
class QPosition(models.Model):
    query = models.ForeignKey(Query , verbose_name=u"Запрос" )
    urls = models.CharField(max_length=65000, verbose_name=u"Url" , null=True , blank=True )
    urls_json = JSONField(verbose_name=u"Позиции", default=list, null=True, blank=True)
    pub_date = models.DateField('Дата снятия' , null=True , blank=True )
    hash = models.CharField(max_length=666 , verbose_name=u"Hash" , unique=True)
    
    def __str__(self):
        return self.hash

class ParseStorage(models.Model):
    class Meta:
        verbose_name = u"Хранилище"
        verbose_name_plural = u"Хранилище"

    on_check = JSONField(verbose_name=u"Позиции", default=list, null=True, blank=True)
    worked_out = JSONField(verbose_name=u"Отработанно", default=list, null=True, blank=True)
    date = models.DateField('Дата снятия', null=True, blank=True)

    def __str__(self):
        return str(self.pk)