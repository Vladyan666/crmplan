import django
import json
import re
import time
import requests
import hashlib
from collections import OrderedDict
import itertools
from operator import itemgetter
from django.db.models import Q
from datetime import datetime, timedelta
from django.http import HttpResponse
from django.template.context_processors import csrf
from django.http.response import Http404
from django.forms.models import model_to_dict
from django.shortcuts import render, render_to_response, get_object_or_404, redirect
from django.template.loader import render_to_string
from django.template import RequestContext, loader
from django.conf import settings as django_settings
from django.core import serializers
from django.contrib.sitemaps import Sitemap
from django.contrib.sitemaps.views import x_robots_tag
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from bs4 import BeautifulSoup

from .models import *

def default_context(request):
    
    c = {}
    c.update(csrf(request))
    
    context_object = {
        'c': c,
    }
    return context_object

def mainpage(request):
    
    context = default_context(request)
    
    template = loader.get_template('top50/index.html')
    
    projects = Project.objects.all()
    
    context.update({
        'projects' : projects
    })
    
    return HttpResponse(template.render(context))
    
def allevents(request):
    
    context = default_context(request)
    
    template = loader.get_template('top50/allevents.html')
    
    events = Event.objects.all().order_by('-id')

    context = RequestContext(request, {
        'events': events,
    })
    
    return HttpResponse(template.render(context))
    
def single_event(request, alias):
    
    context = default_context(request)
    
    template = loader.get_template('top50/single-event.html')
    
    data = get_object_or_404(Event, name=alias)

    context = RequestContext(request, {
        'data': data,
    })
    
    return HttpResponse(template.render(context))

def project(request, alias):
    
    context = default_context(request)
    
    template = loader.get_template('top50/single-project.html')
    
    data = get_object_or_404(Project, title=alias)

    events = Event.objects.all()

    qpages = QueryPage.objects.filter(project=data)

    context = RequestContext(request, {
        'data': data,
        'events': events,
        'qpages': qpages,
    })
    
    return HttpResponse(template.render(context))

def qpositions(request, alias):

    context = default_context(request)
    
    template = loader.get_template('top50/qpositions.html')

    query = get_object_or_404(Query, id=alias)
    qpositions = QPosition.objects.filter(query=query).order_by('-pub_date')[:10]
    projects = Project.objects.all()

    context = RequestContext(request, {
        'qpositions': qpositions,
        'projects': projects,
    })
    
    return HttpResponse(template.render(context))

def project_queries(request, alias):
    context = default_context(request)
    
    template = loader.get_template('top50/project-queries.html')
    
    data = get_object_or_404(Project, title=alias)

    qpages = QueryPage.objects.filter(project=data, query__text__in=Event.objects.filter(active=True).values_list('queries__text', flat=True))
    
    dates = []
    count = 0
    while True:
        dates.append(datetime.now() - timedelta(days=count))
        count += 1
        if count == 30:
            break
            
    graph = []
    for d in dates:
        all_count = qpages.filter(positions__pub_date=d).count()
        if all_count:
            top_3 = qpages.filter(positions__pub_date=d, positions__position__lte=4).count()
            top_10 = qpages.filter(positions__pub_date=d, positions__position__lte=11).count()
            top_30 = qpages.filter(positions__pub_date=d, positions__position__lte=31).count()
            graph.append({"top_3" : 100 / all_count * top_3, "top_10" : 100 / all_count * top_10, "top_30" : 100 / all_count * top_30, "date" : d})

    context = RequestContext(request, {
        'data': data,
        'graph': graph,
        'dates': dates,
        'qpages': qpages,
    })
    
    return HttpResponse(template.render(context))


def parse_iterator(query):
    while True:
        #запрос о позициях к сломанному xml
        qq = requests.get('https://yandex.ru/search/xml?user=Faggio88&key=03.53591640:e25913a3a153acc09c2e171afe9ecc08&lr=213&l10n=ru&sortby=rlv&query=' + query)
        bs = BeautifulSoup(''.join(qq.text))
        #проверка че пришло 
        if len(bs.findAll('group')):
            return bs
#         elif bs.find('error')['code'] == '32':
#             return 'limits off!'

def top50parse(request):
    #удаляем кал
    
    ParseStorage.objects.filter(~Q(date=datetime.now())).delete()
    
    #собираем запросы которые будем обрабатывать
    
    qqq = []
    for i in Event.objects.filter(active=True):
        for q in i.queries.all():
            qqq.append(q.text)
    on_check = ParseStorage.objects.get_or_create(date=datetime.now(), defaults={'on_check': qqq})[0]
    on_check.on_check = qqq


    #запускаем!
    for q in on_check.on_check:
        req = parse_iterator(q)
        domains_list = []
        for url in req.findAll('group'):
            domains_list.append(url.find('url').text)
        q_obj = Query.objects.get(text=q)
        obj, upd = QPosition.objects.update_or_create(query=q_obj, pub_date=datetime.now(), hash=hashlib.md5(q.encode('utf8') + datetime.now().strftime("%Y-%m-%d").encode('utf8')).hexdigest(), defaults={'urls_json': domains_list})
        
        
        #создаем позиции "проект + запрос"
        count = 1
        pub_date = obj.pub_date
        
        
        #создаем пустые посититонс
        all_qpages = QueryPage.objects.filter(query=obj.query)
        qpages_to_created = []
        
        for qpage in all_qpages:
            if not Position.objects.filter(hash=hashlib.md5(qpage.project.title.encode('utf8') + qpage.query.text.encode('utf8') + pub_date.strftime('%Y-%m-%d').encode('utf8')).hexdigest()).count():
                qpages_to_created.append(Position(query=qpage, pub_date=pub_date, position=666, url=':javascript',
                hash=hashlib.md5(qpage.project.title.encode('utf8') + qpage.query.text.encode('utf8') + pub_date.strftime('%Y-%m-%d').encode('utf8')).hexdigest()))
                
        Position.objects.bulk_create(qpages_to_created)
        
        for i in obj.urls_json:
            try:
                project_name = i.split('/')[2]
                project = Project.objects.get(title=project_name)
                querypage = QueryPage.objects.get(query=obj.query, project=project)
                position = count
                hash = hashlib.md5(project.title.encode('utf8') + obj.query.text.encode('utf8') + obj.pub_date.strftime('%Y-%m-%d').encode('utf8')).hexdigest()
                Position.objects.filter(hash=hash, additional=None).update(query=querypage, pub_date=pub_date, url=i, position=position, additional="")
            except Project.DoesNotExist:
                pass
               
            except QueryPage.DoesNotExist:
                pass
               
            finally:
                count += 1
                pass
                