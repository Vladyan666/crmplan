from django.apps import AppConfig


class Top50Config(AppConfig):
    name = 'top50'
