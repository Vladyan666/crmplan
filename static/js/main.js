// --- CSRFMIDDLEWARETOKEN START
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});
// --- CSRFMIDDLEWARETOKEN END


var current_template = null;

function notifySuccess(message) {
    $.notify(message, {
        showAnimation: "fadeIn",
        hideAnimation: "fadeOut",
        arrowShow: false,
        className: "success",
    });
}
const dataTableTaskLanguageOptions = {
    "decimal":        "",
    "emptyTable":     "В таблице пусто",
    "info":           "Showing _START_ to _END_ of _TOTAL_ entries",
    "infoEmpty":      "В таблице пусто",
    "infoFiltered":   "(filtered from _MAX_ total entries)",
    "infoPostFix":    "",
    "thousands":      ",",
    "lengthMenu":     "Показывать _MENU_ задач",
    "loadingRecords": "Загрузка...",
    "processing":     "В процессе...",
    "search":         "Фильтр:",
    "zeroRecords":    "По запросу ничего не найдено",
    "paginate": {
        "first":      "Первая",
        "last":       "Последняя",
        "next":       "Следующая",
        "previous":   "Предыдущая"
    },
};
const formatStatus = (column) => {
    switch (column.text()) {
        case 'Создана':
            column.removeClass('status-inactive')
            column.removeClass('status-test');
            column.removeClass('status-in-progress');
            column.addClass('status-active');
            break;
        case 'Закрыта':
            column.removeClass('status-active')
            column.removeClass('status-test');
            column.removeClass('status-in-progress');
            column.addClass('status-inactive');
            break;
        case 'В процессе':
            column.removeClass('status-active');
            column.removeClass('status-test');
            column.removeClass('status-inactive');
            column.addClass('status-in-progress');
            break;
        case 'Тест':
            column.removeClass('status-active')
            column.removeClass('status-inactive');
            column.removeClass('status-in-progress');
            column.addClass('status-test');
            break;
    }

}

const options = {
    'info': false,
    "language": dataTableTaskLanguageOptions,
    'createdRow': function (row, data, index) {
        $(row).addClass('selectable fs-14');
        $(row).attr('data-attr-key', data.pk);
        $(row).attr('data-attr-executor', data.executor)

        $(row).find('td.to').attr('data-attr-key', data.task_to_pk);
        $(row).find('td.from').attr('data-attr-key', data.task_from_pk);

    

        formatStatus($(row).find('td.status'));
    },
    "order": [[1, 'asc']]
}
const selectedBlock = $('.selected-block');
const projectItemsSelect = '<option></option>{{each(index, proj) data}} <option value="${proj.pk}">${proj.name}</option> {{/each}}';
const userItemsSelect = '<option></option>{{each(index, user) data}} <option value="${user.pk}">${user.name} ${user.surname}</option> {{/each}}';
const forms = {
    'project_form': '#create_project_form',
    'user_form': '#create_user_form',
    'task_form': '#create_task_form'
}


$(function() {
    const selectProject = $('select[name=select_project]')

    function init() {
        renderCurrentProfile();
        refreshProjectSelector()
        refreshUserSelector();

        $('.create_task_deadline').datepicker({ language: "ru" }).on('changeDate', function() {
            $(this).datepicker('hide');
        });

        $('a[data-action=project_list]').click(function() { renderProjectListPage() })
        $('a[data-action=users_list]').click(function() { renderUserListPage() })
        $('#create_task_button').click(function() { refreshProjectSelector() })

        const selectProjectInTaskModal = $('select[name=select_task_project]');
        const selectProjectContributor = $('select[name=select_task_contributor]');
    
        selectProjectInTaskModal.select2({ theme: "bootstrap", placeholder: "Выберите проект" })
        $('select.select2-custom').select2({ theme: "bootstrap", placeholder: "Выберите", })

        $('.navbar-brand').on('click', function() {
            renderCurrentProfile();
            selectProject.val('').trigger('change')
        })

        selectProject.select2({ theme: "bootstrap", placeholder: "Выберите проект" }).on('select2:select', function(e) {
            var selectedOption = e.params.data;
            var selectedOptionKey = selectedOption.id;

            renderProjectPage(selectedOptionKey);
        });

        $(forms['project_form']).on('submit', function(e) {
            e.preventDefault();
    
            var form = $(this);
            var submitButton = form.find('button[type=submit]');
            submitButton.prop("disabled", true);
    
            var projectName = $('input[name=project_name]').val(),
                projectDescription = $('input[name=project_description]').val()
    
            if (projectName) {
                $.ajax({ url: '/createproject/', method: "POST", data: { 'project_name': projectName, 'project_description': projectDescription },
                    success: function (data) {
                        $('#create_project').modal('hide')
                        renderProjectListPage()
                        refreshProjectSelector()
                        submitButton.prop("disabled", false);
                        notifySuccess("Проект успешно создан!");
                    }
                })
            }
        });
        $(forms['user_form']).on('submit', function(e) {
            e.preventDefault();
    
            var form = $(this);
    
            var submitButton = form.find('button[type=submit]')
            submitButton.prop('disabled', true)
    
            var login = form.find('input[name=login]').val(),
                password = form.find('input[name=password]').val(),
                username = form.find('input[name=name]').val(),
                surname = form.find('input[name=surname]').val(),
                email = form.find('input[name=email]').val(),
                phone = form.find('input[name=phone]').val(),
                info = form.find('textarea[name=user_info]').val()
    
            $.ajax({ url: "/createuser/", method: "POST", data: { login, password, username, surname, email, phone, info }, 
                success: function(data) {
                    submitButton.prop("disabled", false);
    
                    if (data === 'ok') {
                        $('#create_user').modal('hide');
                        notifySuccess("Пользователь успешно создан!");
                        refreshUserSelector();
                    }
                }
            })
        })

        $(forms['task_form']).on('submit', function(e) {
            e.preventDefault();
    
            var form = $(this);
    
            var submitButton = form.find('button[type=submit]');
            submitButton.prop("disabled", true);
    
            var projectName = form.find('input[name=task_name]').val();
            var taskProject = selectProjectInTaskModal.val()
            var projectContributor = selectProjectContributor.val()
            var projectFinishDate = $('.create_task_deadline').val()
            var taskComment = $('textarea[name=task_comment]').val()
    
            if (projectName && taskProject  && projectContributor && projectFinishDate) {
                $.ajax({ url: "/createtask/", method: "POST", data: { projectName, taskProject, projectContributor, projectFinishDate, taskComment }, 
                    success: function(data) {
                        submitButton.prop("disabled", false);
    
                        if (data === 'ok') {
                            $('#create_task').modal('hide');
                            notifySuccess("Задача успешно создана!");
                            renderCurrentProfile()
                        }
                    }
                })
            }
        })
    }

    init();
    
    $.template('project_items_select', projectItemsSelect);
    $.template('user_items_select', userItemsSelect);

    function getAllSelectedTableRows(table_el) {
        var indices = []

        $(table_el + ' > tbody > tr.selected').each(function(index, row) { indices.push($(row).attr('data-attr-key')); })
        return indices;
    }
    function getCountOfSelectedItemsInTable(table_el) {
        var count = 0;

        $(table_el + ' > tbody > tr.selected').each(function(index, row) { count++; });
        return count;
    }
    function setSelectableTableRows(table, rm_btn) {
        $(table + ' tbody').on('click', 'td.details-control', function(e) {
            e.stopPropagation();

            $(this).parent().toggleClass('selected');

            getCountOfSelectedItemsInTable(table) > 0 ? rm_btn.removeClass('d-none') : rm_btn.addClass('d-none');
        });
    }
    function setClickableProfile(table) {
        $(table + ' tbody').on('click', 'td.profile-details', function(e) {
            e.stopPropagation();

            var profileId = $(this).attr('data-attr-key');

            renderAnotherProfile(profileId);
        })
    }
    function setClickableProject(table) {
        $(table + ' tbody').on('click', 'td.project-details', function(e) {
            e.stopPropagation();

            var projectId = $(this).parents('tr').attr('data-attr-key');

            renderProjectPage(projectId);
        })
    }
       

    /**
     *  --------------------------------------------------------------     
     *  <!-------------------- FETCH METHODS ------------------------>
     *  -------------------------------------------------------------- */
    function fetchUsers() {
        return Promise.resolve($.ajax({ url: "/getusers/", method: "GET", 
            success: function(data) {
                if (data !== 'error') {
                    return data;
                }
            }
        }));
    }
    function fetchProjectData(id) {
        return Promise.resolve($.ajax({ url: "/getproject/", method: "GET", data: { 'selectedOptionKey': id },
            success: function (data) {
                if (data !== 'error') {
                    return data;
                }
            }    
        }));
    }

    function fetchProjectList() {
        return Promise.resolve($.ajax({ url: "/getprojects/", method: "GET",
            success: function (data) {
                if (data !== 'error') {
                    return data;
                }
            }    
        }));
    }

    function fetchCurrentProfileData() {
        return Promise.resolve($.ajax({ url: "/getselfprofile/", method: "GET",
            success: function (data) {
                if (data !== 'error') {
                    return data;
                }
            }    
        }));
    }
    function fetchProfileData(id) {
        return Promise.resolve($.ajax({ url: "/getprofile/", method: "GET", data: {id},
            success: function (data) {
                if (data !== 'error') {
                    return data;
                }
            }
        }))
    }
    function fetchStatusesById(indices) {

        return Promise.resolve($.ajax({
            url: "/getstatus/",
            method: "POST", 
            contentType: "application/json; charset=utf-8",
            dataType: "json", 
            data:JSON.stringify({ 'indices': indices }),
            success: function(data) {
                if (data !== 'error') {
                    return data;
                }
            }
        }))
    }
    // <!--------------------- END FETCH METHODS ---------------------->

    function format ( d ) {
        return '<div class="slider">' +
            `<div class="card text-white bg-row-child fs-13">
                <div class="card-body pl-0 column-1">
                    <div class="row m-0">
                        <div class="col-6 child-col-section pb-2">
                            <div class="row">
                                <div class="col-4"><p class="card-text">Кто назначил:</p></div>
                                <div class="col-8"><p class="card-text">${d.task_from} ${d.task_from_surname}</p></div>
                            </div>
                            <div class="row">
                                <div class="col-4"><p class="card-text">Кому назначили:</p></div>
                                <div class="col-8"><p class="card-text">${d.task_to} ${d.task_to_surname}:</p></div>
                            </div>
                            <div class="row">
                                <div class="col-4"><p class="card-text">Создание:</p></div>
                                <div class="col-8"><p class="card-text">${d.create_date}</p></div>
                            </div>
                            <div class="row">
                                <div class="col-4"><p class="card-text">Дедлайн: <i class="fa fa-edit ml-2" id="edit_task_deadline"></i></p></div>
                                <div class="col-8"><p class="card-text child_deadline">${d.finish_date}</p></div>
                            </div>
                        </div>
                        <div class="col-6 child-col-section pb-2">
                            <div class="row">
                                <div class="col-4"><p class="card-text">Описание: <i class="fa fa-edit ml-2" id="edit_task_desc"></i></p></div>
                                <div class="col-8"><p class="card-text child_additional_info">${d.description}</p></div>
                            </div>
                            <div class="row">
                                <div class="col-4"><p class="card-text">Комментарий: <i class="fa fa-edit ml-2" id="edit_task_comment"></i></p></div>
                                <div class="col-8"><p class="card-text child_comment">${d.comment}</p></div>
                            </div>
                            <div class="row">
                                <div class="col-4"><p class="card-text">Прикрепленные:</p></div>
                                <div class="col-8">
                                    <ul class="list-style-none">
                                        <li><a href="#" class="nav-link">test</a></li>
                                        <li><a href="#" class="nav-link">test</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <p class="text-white text-center">Чат [progress]</p>
                        </div>
                    </div>
                </div>
            </div>` +
        '</div>';
    }

    /**
     *  --------------------------------------------------------------
     *  <!-------------------- RENDER METHODS ------------------------>
     *  --------------------------------------------------------------  */ 

    async function refreshProjectSelector() {
        var data = null;

        var selectors = [$('#project_selector'), $('#main_project_selector')]

        try {
            data = await fetchProjectList();

            selectors.forEach((selector) => {
                selector.html('');
                $.tmpl('project_items_select', { 'data': data }).appendTo(selector);
            })
        } catch (error) { console.log(error); }
    }
    async function refreshUserSelector() {
        var data = null;
        var selectors = [$('select[name=select_task_contributor]')];
        
        try {
            data = await fetchUsers();

            selectors.forEach((selector) => {
                selector.html('');
                $.tmpl('user_items_select', { 'data': data }).appendTo(selector);
            })
        } catch (error) { console.log(error); }
    }
    function setTaskWatched(row_task, id) {
        $.ajax({ 
            method: "POST",
            url: "/updatetaskwatch/",
            data: {id},
            success: function(data) {
                if (data === 'ok') {
                    let status_column = row_task.find('.status')
                    status_column.text('В процессе')
                    formatStatus(status_column)
                }
            }
        })
    }
    function setChildOnTable(templateData) {
        $(templateData['table_name'] + ' tbody').on('click', 'tr.selectable', function (e) {
            var table = templateData['table'];
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (tr.find('.status').text() === 'Создана' && tr.attr('data-attr-executor') === "1") {
                setTaskWatched(tr, tr.attr('data-attr-key'))
            }

            if (row.child.isShown()) {
                $('div.slider', row.child()).slideUp(function() {
                    row.child.hide();
                    tr.removeClass('shown');
                });
            }
            else {
                if (table.row('.shown').length) {
                   $(table.row('.shown').node()).click();
                }

                row.child(format(row.data()), 'no-padding').show();
                row.child().attr('data-attr-key', tr.attr('data-attr-key'))
                row.child().find('td').addClass('p-0')

                tr.addClass('shown');
                
                $('div.slider', row.child()).slideDown();

                var previousComment = null, 
                    previousDescription = null,
                    previousDeadline = null;

                $('#edit_task_desc').on('click', function(event) {
                    event.stopPropagation();

                    var currentRow = $(this).closest('tr');
                    var selector = currentRow.find('.child_additional_info');

                    if (selector.find('textarea').length) {                        
                        let task_id = currentRow.attr('data-attr-key');
                        let description = selector.find('textarea').val();

                        if (previousDescription !== description) {
                            $.ajax({ method: "POST", url: "/updatetaskdescription/", data: { task_id, description }, 
                                success: function(data) {
                                    if (data !== 'error') {
                                        notifySuccess("Успешно сохранено!");
                                    }
                                }
                            })
                        } 

                        selector.text(description);
                    } else {
                        var t = selector.text();
                        previousDescription = t;
                        selector.html($('<textarea />',{'text' : t, 'class': 'form-control'}).val(t));
                        selector.find('textarea').focus()
                    }
                })
                $('#edit_task_deadline').on('click', function(event) {
                    event.stopPropagation();

                    var currentRow = $(this).closest('tr');
                    var selector = currentRow.find('.child_deadline');

                    if (selector.find('input').length) {                        
                        let id = currentRow.attr('data-attr-key');
                        let deadline = selector.find('input').val();

                        if (previousDeadline !== deadline) {
                            $.ajax({ method: "POST", url: "/updatetaskdeadline/", data: { id, deadline }, 
                                success: function(data) {
                                    if (data !== 'error') {
                                        notifySuccess("Успешно сохранено!");
                                    }
                                }
                            })
                        } 

                        selector.text(deadline);
                    } else {
                        var t = selector.text();
                        previousDeadline = t;
                        selector.html($('<input />',{'text' : t, 'class': 'form-control'}).val(t));
                        var inpt = selector.find('input')
                        inpt.datepicker({ language: "ru" }).on('changeDate', function() {
                            $(this).datepicker('hide');
                        });
                        inpt.focus()
                    }
                })
                $('#edit_task_comment').on('click', function(event) {
                    event.stopPropagation();

                    var currentRow = $(this).closest('tr');
                    var selector = currentRow.find('.child_comment');

                    if (selector.find('textarea').length) {
                        let task_id = currentRow.attr('data-attr-key');
                        let comment = selector.find('textarea').val();

                        if (previousComment !== comment) {
                            $.ajax({ method: "POST", url: "/updatecomment/", data: { task_id, comment },
                                success: function(data) {
                                    if (data !== 'error') {
                                        notifySuccess('Успешно сохранено!');
                                    }
                                }
                            })
                        }
                        selector.text(comment);                          
                    } else {
                        var t = selector.text();
                        previousComment = t;
                        selector.html($('<textarea />', {'text' : t, 'class': 'form-control'}).val(t));
                        selector.find('textarea').focus()
                    }
                });
            }
        } );
    }
    function setSuperRightsToChildRow() {
        
    }
    async function renderAnotherProfile(id) {
        let data = null

        try {
            data = await fetchProfileData(id);
        } catch (error) {
            console.log('Error', error);
        }

        setTemplate('profile', data, () => {
            const tableName = '#profile_tasks_table';
            const pointedTableName = "#profile_pointed_tasks_table";

            const table = $(tableName).DataTable(Object.assign({}, options, {
                "data": data.tasks,
                "columns": [
                    {
                        "className":      'details-control',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": ''
                    },
                    { "data": "name" },
                    { "data": "status", "className": 'status' },
                    { "data": "project" },
                    { "data": "task_from", 'className': 'profile-details from' },
                    { "data": "create_date" },
                    { "data": "finish_date" }
                ],
            }));

            const pointedTasks = $(pointedTableName).DataTable(Object.assign({}, options, {
                "data": data.pointed_tasks,
                "columns": [
                    {
                        "className":      'details-control',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": ''
                    },
                    { "data": "name" },
                    { "data": "status", "className": 'status' },
                    { "data": "project" },
                    { "data": "task_to", 'className': 'profile-details to' },
                    { "data": "create_date" },
                    { "data": "finish_date" }
                ]
            }));

            templateData = [
                {
                    'table': table,
                    'table_name': tableName
                },
                {
                    'table': pointedTasks,
                    'table_name': pointedTableName
                }
            ]

            templateData.forEach((table) => {
                setClickableProfile(table['table_name']);
                setChildOnTable(table);
            })
        });
    }

    var profileTasksInterval = null;
    var profilePointedTasksInterval = null;

    async function renderCurrentProfile() {
        current_template = 'current_profile';
        let data = null;

        try {
            data = await fetchCurrentProfileData();
            console.log(data);
        } catch (error) {
            console.log('Error:', error);
        }

        setTemplate('profile', data, () => {
            const tableName = '#profile_tasks_table';
            const pointedTableName = "#profile_pointed_tasks_table";

            const table = $(tableName).DataTable(Object.assign({}, options, {
                // "ajax": { "url": "/getselfprofile/", dataSrc: function(d) {return d.tasks} },
                "data": data.tasks,
                "columns": [
                    {
                        "className":      'details-control',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": ''
                    },
                    { "data": "name" },
                    { "data": "status", "className": 'status' },
                    { "data": "project" },
                    { "data": "task_from", "className": 'profile-details from' },
                    { "data": "create_date" },
                    { "data": "finish_date" }
                ],
            }));

        
            const pointedTasks = $(pointedTableName).DataTable(Object.assign({}, options, {
                // "ajax": { "url": "/getselfprofile/", dataSrc: function(d) {return d.pointed_tasks} },
                "data": data.pointed_tasks,
                "columns": [
                    {
                        "className":      'details-control',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": ''
                    },
                    { "data": "name" },
                    { "data": "status", "className": 'status' },
                    { "data": "project" },
                    { "data": "task_to", "className" : 'profile-details to' },
                    { "data": "create_date" },
                    { "data": "finish_date" }
                ]
            }));

            const templateData = [
                {
                    'remove_button': $('#remove_tasks'),
                    'table_name': tableName,
                    'table': table,
                    'data': data.tasks,
                    'interval': profileTasksInterval,
                },
                {
                    'remove_button': $('#remove_pointed_tasks'),
                    'table_name': pointedTableName,
                    'table': pointedTasks,
                    'data': data.pointed_tasks,
                    'interval': profilePointedTasksInterval,
                }
            ]
    
            templateData.forEach((table) => {
                setSelectableTableRows(table['table_name'], table['remove_button']);
                setClickableProfile(table['table_name']);
                setChildOnTable(table)
                setSuperRightsToChildRow();

                var tableSelector = table['table'];
                var tableName = table['table_name'];
                var rm_button = table['remove_button'];

                rm_button.click(function() {
                    $(this).addClass('d-none');
        
                    $.ajax({ method: "POST", url: "/removetasks/", contentType: "application/json; charset=utf-8", dataType: "json", 
                        data: JSON.stringify({ 'indices': getAllSelectedTableRows(table['table_name']) }),
                        success: function(data) {
                            if (data === 'ok') {
                                renderCurrentProfile()
                                notifySuccess("Удаление успешно!");
                            }
                        }
                    })
                });

                $(tableName + ' tbody').on('click', 'td.status', function(e) {
                    e.stopPropagation();
        
                    var column = $(this)
                    var row = column.parent();
                    var taskId = row.attr('data-attr-key');
                    var taskStatus = column.text();

                    $.ajax({ method: "POST", url: "/updatestatus/", data: { taskId, taskStatus }, 
                        success: function(data) {
                            if (data !== 'error') {
                                if (taskStatus !== data) {
                                    column.text(data);
                                    formatStatus(column);
                                    notifySuccess("Статус успешно изменен!");
                                }
                            }
                        }
                    })
                })
            });

            var taskStatuses = data.tasks.map((task) => task.pk)
            var pointedTaskStatuses = data.pointed_tasks.map((task) => task.pk)
            
            clearInterval(profileTasksInterval)
            clearInterval(profilePointedTasksInterval)
            // profileTasksInterval = setStatusAutoUpdate(tableName, taskStatuses, profileTasksInterval, 'current_profile')
            // profilePointedTasksInterval = setStatusAutoUpdate(pointedTableName, pointedTaskStatuses, profilePointedTasksInterval, 'current_profile')

            profileTasksInterval = setInterval(() => {
                if (current_template !== 'current_profile') clearInterval(profileTasksInterval)

                updateStatuses(tableName, taskStatuses)
            }, 5000)

            profilePointedTasksInterval = setInterval(() => {
                if (current_template !== 'current_profile') clearInterval(profilePointedTasksInterval)

                updateStatuses(pointedTableName, pointedTaskStatuses)
            }, 5000)
        })
    }
    async function setStatusAutoUpdate(tableName, statuses, interval = null, template) {

            var interval = setInterval(() => { 
                updateStatuses(tableName, statuses) 

                if (current_template !== template) {
                    clearInterval(interval);
                }
            } , 5000);
        
        return interval;
    }

    function clearIntervals() {
        clearInterval(profileTasksInterval)
        clearInterval(profilePointedTasksInterval)

        profileTasksInterval = null, profilePointedTasksInterval = null;
    }
    async function updateStatuses(tableName, statuses) {
        updatedStatuses = await fetchStatusesById(statuses);
    
        if (updatedStatuses.length > 0) {
            $(tableName).find('td.status').each(function(index) {
                var td = $(this);
                var id = td.parents('tr').attr('data-attr-key');

                updatedStatuses.forEach((task) => {
                    if (parseInt(task.pk) === parseInt(id)) {
                        status = task.status
                        td.text(task.status)
                        formatStatus(td)
                        return;
                    }
                })  
            })
        }
    }
    async function renderProjectPage(id) {
        current_template = 'project';
        var data = null;

        try {
            data = await fetchProjectData(id);
        } catch (error)  { console.log('Error:', error); }

        setTemplate('project_template', data, () => {
            const tableName = '#project_tasks_table';

            const table = $('#project_tasks_table').DataTable(Object.assign({}, options, {
                "data": data.tasks,
                "columns": [
                    {
                        "className":      'details-control',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": ''
                    },
                    { "data": "name" },
                    { "data": "status", "className": 'status' },
                    { "data": "task_to", "className": "profile-details to" },
                    { "data": "task_from", "className": "profile-details from" },
                    { "data": "create_date" },
                    { "data": "finish_date" }
                ],
            }));

            const templateData = {
                'remove_button': $('#remove_tasks'),
                'table_name': tableName,
                'table': table,
            }
    
            setSelectableTableRows(templateData['table_name'], templateData['remove_button']);
            setClickableProfile(templateData['table_name']);
            setChildOnTable(templateData);

            templateData['remove_button'].click(function() {
                $(this).addClass('d-none');
    
                $.ajax({ method: "POST", url: "/removetasks/", contentType: "application/json; charset=utf-8", dataType: "json", 
                    data: JSON.stringify({ 'indices': getAllSelectedTableRows(templateData['table_name']) }),
                    success: function(data) {
                        if (data === 'ok') {
                            renderProjectPage(id)
                            notifySuccess("Удаление успешно!");
                        }
                    }
                })
            });

            var edit_description = $('#edit_description');

            $('.project-data-description').on('click', function(event) {
                event.stopPropagation();

                if ($(this).find('textarea').length < 1) { 
                    edit_description.removeClass('d-none');

                    var t = $(this).text();
                    $(this).html($('<textarea />', {'text' : t, 'class': 'form-control'}).val(t));
                    $(this).find('textarea').focus()
                }
            });

            edit_description.on('click', function() {
                $(this).addClass('d-none');

                var projectDataDescription = $('.project-data-description')

                if (projectDataDescription.find('textarea').length) {
                    let id = projectDataDescription.attr('data-attr-key');
                    let description = projectDataDescription.find('textarea').val()

                    projectDataDescription.text(description);
                    $.ajax({ method: "POST", url: "/updatedescription/", data: { id, description },
                        success: function(data) {
                            if (data !== 'error') {
                                notifySuccess('Успешно сохранено!');
                            }
                        }
                    })
                }
            })

            const removeProjectBtn = $('#delete_project');
            removeProjectBtn.on('click', function() {
                $.ajax({
                    method: "POST",
                    url: "/removeprojects/", 
                    contentType: "application/json; charset=utf-8", 
                    dataType: "json", 
                    data: JSON.stringify({ 'indices': [removeProjectBtn.data('attr-id')] }),
                    success: function(data) {
                        if (data === 'ok') {
                            renderProjectListPage();
                            refreshProjectSelector();
                            notifySuccess("Удаление успешно!");
                        }
                    }
                })
            });
        })
    }

    function setTemplate(template_name, data, callback) {
        clearIntervals()
        let url = '/static/templates/' + template_name + '.html';

        $.get({ url, cache: false }).then(function(templateBody) {
            selectedBlock.html('');
            $.tmpl(templateBody, { data }).appendTo(selectedBlock);

            callback();
        })
    }


     // <!--- PROJECT LIST TEMPLATE --->
    async function renderProjectListPage() {
        current_template = 'project_list';
        var data = null;

        try {
            data = await fetchProjectList();
        } catch (error) {
            console.log('Error:', error);
        }

        setTemplate('project_list', data, () => {
            const tableName = '#projects_table';

            $(tableName).DataTable({
                'info': false,
                "language": dataTableTaskLanguageOptions,
                'createdRow': function (row, data, index) {
                    $(row).addClass('selectable fs-14');
                    $(row).attr('data-attr-key', data.pk);
                },
                "order": [[1, 'asc']],
                "data": data,
                "columns": [
                    {
                        "className":      'details-control',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": ''
                    },
                    { "data": "name", "className": "project-details" },
                    { "data": "description", "className": "project-details" }
                ]
            });

            var remove_button = $('#remove_projects');

            remove_button.click(function() {
                remove_button.addClass('d-none');

                $.ajax({
                    method: "POST",
                    url: "/removeprojects/", 
                    contentType: "application/json; charset=utf-8", 
                    dataType: "json", 
                    data: JSON.stringify({ 'indices': getAllSelectedTableRows('#projects_table') }),
                    success: function(data) {
                        if (data === 'ok') {
                            renderProjectListPage();
                            refreshProjectSelector();
                            notifySuccess("Удаление успешно!");
                        }
                    }
                })
            });

            setSelectableTableRows(tableName, remove_button);
            setClickableProject(tableName);
        });  
    }
    async function renderUserListPage() {
        current_template = 'user_list';
        var data = null;

        try {
            data = await fetchUsers();
            console.log(data);
        } catch (error) {
            console.log('Error:', error);
        }

        setTemplate('users_template', data, () => 
        {
            const tableName = '#users_table';
            const table = $(tableName).DataTable({
                'info': false,
                "language": dataTableTaskLanguageOptions,
                "data": data,
                "columns": [
                    {
                        "className":      'details-control',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": ''
                    },
                    { 
                        "data": function (data, type, dataToSet) {
                            return data.name + " " + data.surname;
                        },
                        "className": "profile-details",
                    },
                    { "data": "email" },
                    { "data": "info" },
                ],
                'createdRow': function (row, data, index) {
                    $(row).addClass('selectable fs-14');
                    $(row).attr('data-attr-key', data.pk);
                },
            });

            $(tableName + ' tbody').on('click', 'td.profile-details', function(e) {
                e.stopPropagation();
    
                var profileId = $(this).closest('tr').attr('data-attr-key');
    
                renderAnotherProfile(profileId);
            })

            var remove_button = $('#remove_users');

            remove_button.click(function() {
                $(this).addClass('d-none');

                $.ajax({ 
                    method: "POST",
                    url: "/removeuser/",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ 'indices': getAllSelectedTableRows('#users_table') }),
                    success: function(data) {
                        if (data === 'ok') {
                            renderUserListPage();
                            refreshUserSelector();
                            notifySuccess("Удаление успешно!")
                        }
                    }
                })
            })

            setSelectableTableRows(tableName, remove_button);
        })
    }
    // </------------------------------ END RENDER METHODS ------------------------------->
})